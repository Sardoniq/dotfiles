# Sardoniq dotfiles

## Name
Sardoniq dotfiles

## Description
A collection of my configuration files.

## Vim
1. Install [Vundle](https://github.com/VundleVim/Vundle.vim).

`git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim`

2. Copy raw .vimrc to .vim directory.

`wget https://gitlab.com/Sardoniq/dotfiles/-/raw/main/.vimrc -O ~/.vim/vimrc`

3. Run vim and type

`:PackageInstall`

## Arkenfox
1. Find Firefox profile root directory
* Navigate to `about:support`
* Click "Open Directory" (Application Basics -> Profile Directory -> Open Directory)

2. Backup entire folder. When copying your profile directory, make sure Firefox is closed. Rename your copied folder to something meaningful, and keep the original name (for an easy rollback). For example, if the profile folder is called `jiu9k8gm.default`, a backup copy might be called `jiu9k8gm.default-backup` or `jiu9k8gm.default-pre-arkenfox-userjs`.

3. Open a terminal and `cd` inside root profile folder.

4. Grab Arkenfox shell scripts and userjs:

`wget https://raw.githubusercontent.com/arkenfox/user.js/master/prefsCleaner.sh`

`wget https://raw.githubusercontent.com/arkenfox/user.js/master/updater.sh`

`wget https://raw.githubusercontent.com/arkenfox/user.js/master/user.js`


5. Copy user-overrides.js into the profile folder:

` wget https://gitlab.com/Sardoniq/dotfiles/-/raw/main/user-overrides.js`

6. Run updater.sh to append user-overrides.js to end of user.js

7. Run prefsCleaner.sh

8. (Optional) Install desired plugins (see [Arkenfox recommendations](https://github.com/arkenfox/user.js/wiki/4.1-Extensions)):
* DarkReader
* Password manager
* uBlock Origin (medium or hard blocking mode)
* Skip Redirect 
