// enable session restore
// user_pref("browser.startup.page", 3);
// user_pref("privacy.clearOnShutdown.history", false);

// enable search from urlbar
user_pref("keyword.enabled", true);
user_pref("browser.search.suggest.enabled", true);

// disable saving passwords
user_pref("signon.rememberSignons", false);

// Mullvad DNS over HTTPS (prevent DNS leaks)
user_pref("network.trr.custom_uri", "https://doh.mullvad.net/dns-query");
user_pref("network.trr.mode", 3);
user_pref("network.trr.uri", "https://doh.mullvad.net/dns-query");
